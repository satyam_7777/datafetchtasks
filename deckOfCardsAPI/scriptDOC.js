const http=require('http')
const fs=require('fs')
const port=3043

const server=http.createServer(function(req,res){
res.writeHead(200,{'Content-Type':'text/html' })
fs.readFile('deckOfCardsApi.html',function(error,data){
if(error){
res.writeHead(404)
res.write('error')
}else{
res.write(data)
}
res.end()
})

})
server.listen(port,function(error){
if(error){
console.log('Error is happening',error)
}else{
console.log('port is listening to port '+port)
}
})
